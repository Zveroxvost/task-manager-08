package ru.t1.zvero.tm.repository;

import ru.t1.zvero.tm.api.ICommandRepository;
import ru.t1.zvero.tm.constant.ArgumentConst;
import ru.t1.zvero.tm.constant.CommandConst;
import ru.t1.zvero.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            CommandConst.ABOUT, ArgumentConst.ABOUT,
            "Show developer info."
    );

    private static final Command INFO = new Command(
            CommandConst.INFO, ArgumentConst.INFO,
            "Show system info."
    );

    private static final Command VERSION = new Command(
            CommandConst.VERSION,ArgumentConst.VERSION,
            "Show application version."
    );

    private static final Command HELP = new Command(
            CommandConst.HELP,ArgumentConst.HELP,
            "Show command list."
    );

    private static final Command EXIT = new Command(
            CommandConst.EXIT,null,
            "Close application."
    );

    private static final Command[] COMMANDS = new Command[] {
            ABOUT, INFO, VERSION, HELP, EXIT
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}