package ru.t1.zvero.tm;

import ru.t1.zvero.tm.api.ICommandRepository;
import ru.t1.zvero.tm.constant.ArgumentConst;
import ru.t1.zvero.tm.constant.CommandConst;
import ru.t1.zvero.tm.model.Command;
import ru.t1.zvero.tm.repository.CommandRepository;
import ru.t1.zvero.tm.util.FormatUtil;

import java.util.Scanner;

public final class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(String[] args) {
        processArguments(args);
        processCommand();
    }

    private static void processCommand() {
        final Scanner scanner = new Scanner(System.in);
        System.out.println("** WELCOME TO TASK MANAGER **");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String argument = args[0];
        processArguments(argument);
    }

    private static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.ABOUT:
                showAbout();
                break;
            case CommandConst.VERSION:
                showVersion();
                break;
            case CommandConst.HELP:
                showHelp();
                break;
            case CommandConst.INFO:
                showSystemInfo();
                break;
            case CommandConst.EXIT:
                showExit();
                break;
            default:
                showErrorCommand();
                break;
        }
    }

    private static void processArguments(final String argument) {
        if (argument == null || argument.isEmpty()) return;
        switch (argument) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.INFO:
                showSystemInfo();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            default:
                showErrorArgument();
                break;
        }
        System.exit(0);
    }

    private static void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final long availableProcessors = runtime.availableProcessors();
        final long freeMemory = runtime.freeMemory();
        final String freeMemoryFormat = FormatUtil.format(freeMemory);
        final long maxMemory = runtime.maxMemory();
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = (maxMemoryCheck ? "no limit" : FormatUtil.format(maxMemory));
        final long totalMemory = runtime.totalMemory();
        final String totalMemoryFormat = FormatUtil.format(totalMemory);
        final long usageMemory = totalMemory - freeMemory;
        final String usageMemoryFormat = FormatUtil.format(usageMemory);

        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Free memory: " + freeMemoryFormat);
        System.out.println("Maximum memory: " + maxMemoryFormat);
        System.out.println("Total memory: " + totalMemoryFormat);
        System.out.println("Usage memory: " + usageMemoryFormat);
    }

    private static void showExit() {
        System.out.println("[EXIT]");
        System.exit(0);
    }

    private static void showErrorArgument() {
        System.err.println("[ERROR]");
        System.err.println("this argument not supported...");
    }

    private static void showErrorCommand() {
        System.err.println("[ERROR]");
        System.err.println("this command not supported...");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Elena Volnenko");
        System.out.println("e-mail: elena@volnenko.ru");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.2.0.");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = COMMAND_REPOSITORY.getCommands();
        for (final Command command: commands) System.out.println(command);
    }

}